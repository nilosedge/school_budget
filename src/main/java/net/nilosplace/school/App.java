package net.nilosplace.school;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.*;

import com.jgoodies.forms.builder.FormBuilder;

public class App extends ApplicationFrame {

	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	
	SpinnerModel tuitionIncome = new SpinnerNumberModel(300, 0, 1000, 1);
	SpinnerModel bangorIncome = new SpinnerNumberModel(1200, 0, 5000, 25);
	SpinnerModel otherIncome1 = new SpinnerNumberModel(0, 0, 5000, 50);
	SpinnerModel otherIncome2 = new SpinnerNumberModel(0, 0, 5000, 50);
	SpinnerModel otherIncome3 = new SpinnerNumberModel(0, 0, 5000, 50);
	
	SpinnerModel teachersExpense = new SpinnerNumberModel(1, 0, 5, 1);
	SpinnerModel aidsExpense = new SpinnerNumberModel(0, 0, 5, 1);
	
	SpinnerModel teacherCost = new SpinnerNumberModel(4644, 0, 10000, 1);
	SpinnerModel aidCost = new SpinnerNumberModel(1200, 0, 10000, 10);
	
	CategoryMarker breakEvenLine1 = new CategoryMarker("0");
	CategoryMarker breakEvenLine2 = new CategoryMarker("0");
	
	public App(String title) {
		super(title);

		ControlPanel cp = new ControlPanel();
		tuitionIncome.addChangeListener(cp);
		bangorIncome.addChangeListener(cp);
		otherIncome1.addChangeListener(cp);
		otherIncome2.addChangeListener(cp);
		otherIncome3.addChangeListener(cp);
		teachersExpense.addChangeListener(cp);
		aidsExpense.addChangeListener(cp);
		teacherCost.addChangeListener(cp);
		aidCost.addChangeListener(cp);
		cp.init();

		JFreeChart lineChart = ChartFactory.createLineChart(title, "Students Per Year", "Cost Per Year", dataset, PlotOrientation.VERTICAL, false, false, false);

		//CategoryPlot plot = lineChart.getCategoryPlot();
		//CategoryAxis domainAxis = plot.getDomainAxis();
        //domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
		
		breakEvenLine1.setPaint(Color.black);
		breakEvenLine1.setAlpha(0.25f);
		breakEvenLine1.setDrawAsLine(true);
		breakEvenLine2.setPaint(Color.black);
		breakEvenLine2.setAlpha(0.25f);
		breakEvenLine2.setDrawAsLine(true);
		
		ChartPanel chartPanel = new ChartPanel( lineChart );
		chartPanel.setPreferredSize( new java.awt.Dimension( 800 , 400 ) );

		CategoryPlot plot = (CategoryPlot) chartPanel.getChart().getPlot();
		plot.addDomainMarker(breakEvenLine1);
		plot.addDomainMarker(breakEvenLine2);
		setContentPane( chartPanel );
		updateDataset();

	}

	private void updateDataset() {
		//System.out.println("Update");
		dataset.clear();

		//System.out.println(tuitionIncome.getValue() + " " + bangorIncome.getValue() + " " + teachersExpense.getValue() + " " + aidsExpense.getValue());


		double bangor = (int)bangorIncome.getValue() * 12;

		double teacher = (int)teachersExpense.getValue() * (int)teacherCost.getValue() * 12;

		double aid = (int)aidsExpense.getValue() * 1200 * 10;
		
		double other1 = (int)otherIncome1.getValue() * 12;
		double other2 = (int)otherIncome2.getValue() * 12;
		double other3 = (int)otherIncome3.getValue() * 12;
		
		double addValue = 0, lastValue = 0;
		
		for(int i = 1; i <= 30; i++) {
			double tuition = (int)tuitionIncome.getValue() * 10 * i;
			
			addValue = tuition + bangor + (other1 + other2 + other3) - teacher - aid;
			if(lastValue < 0 && addValue > 0) {
				breakEvenLine1.setKey("" + (i - 1));
				breakEvenLine2.setKey("" + i);
			}
			dataset.addValue(addValue, "schools", i + "");
			dataset.addValue(0, "zero", i + "");
			lastValue = addValue;

		}
	}

	public static void main( String[] args ) {
		App app = new App("School Budget");
		app.pack();
		RefineryUtilities.centerFrameOnScreen(app);
		app.setVisible(true);


	}

	private class ControlPanel implements ActionListener, ChangeListener {

		FormBuilder builder = FormBuilder.create();

		public ControlPanel() { }

		public void init() {

			try {

				JSpinner tuitionIncomeSpin = new JSpinner(tuitionIncome);
				JSpinner bangorIncomeSpin = new JSpinner(bangorIncome);
				JSpinner otherIncome1Spin = new JSpinner(otherIncome1);
				JSpinner otherIncome2Spin = new JSpinner(otherIncome2);
				JSpinner otherIncome3Spin = new JSpinner(otherIncome3);
				
				
				
				JSpinner teachersExpenseSpin = new JSpinner(teachersExpense);
				JSpinner aidExpenseSpin = new JSpinner(aidsExpense);
				JSpinner teacherCostSpin = new JSpinner(teacherCost);
				JSpinner aidCostSpin = new JSpinner(aidCost);

				builder.columns("right:pref, 3dlu, pref, 7dlu, pref, 7dlu, right:pref, 3dlu, pref");
				builder.rows("p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p");

				builder.border(BorderFactory.createEmptyBorder(5, 5, 5, 5));

				builder.addSeparator("Income").xyw(1,  1, 7);

				//builder.add("&Currency").xy(1,  3);
				//builder.add(instrumentBox).xy(3, 3);

				builder.add("&Tuition/m * 10").xy(1,  5);
				builder.add(tuitionIncomeSpin).xy(3,  5);

				builder.add("&Bangor/m * 12").xy(1,  7);
				builder.add(bangorIncomeSpin).xy(3,  7);

				builder.add("&Other1/m * 12").xy(1,  9);
				builder.add(otherIncome1Spin).xy(3,  9);

				builder.add("&Other2/m * 12").xy(1,  11);
				builder.add(otherIncome2Spin).xy(3,  11);
				
				builder.add("&Other3/m * 12").xy(1,  13);
				builder.add(otherIncome3Spin).xy(3,  13);
				
				builder.addSeparator("Expense").xyw(1,  15, 7);

				builder.add("&Teacher Count").xy(1,  17);
				builder.add(teachersExpenseSpin).xy(3,  17);

				builder.add("&Aid Count").xy(1,  19);
				builder.add(aidExpenseSpin).xy(3,  19);

				builder.addSeparator("Teacher").xyw(1,  21, 7);
				
				builder.add("&Teacher Cost Per Month").xy(1,  23);
				builder.add(teacherCostSpin).xy(3,  23);
				
				builder.add("&Aid Cost Per Month").xy(1,  25);
				builder.add(aidCostSpin).xy(3,  25);
				
				//builder.add("&Trade2").xy(1,  17);
				//builder.add(trade2).xy(3,  17);

				//builder.add("&Info").xy(1,  19);
				//builder.add(trade2info).xy(3,  19);

				JFrame frame = new JFrame("School Budget");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(builder.build());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Override
		public void actionPerformed(ActionEvent e) {
			updateDataset();
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			updateDataset();
		}

	}
}
